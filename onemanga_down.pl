#!/usr/bin/perl
# onemanga_down.pl : A script to download manga from onemanga
#
# Released under ISC license.
#
# Copyright (c) 2008, Abel Camarillo <00z@the00z.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

use WWW::Mechanize;
use Getopt::Long ;

Getopt::Long::Configure('bundling');

# default options
my ($debug, $url, $base, $dst_dir) = (0, '', '', '');

GetOptions('debug|d'            => \$debug
    , 'url|u=s'                 =>\$url
    , 'dest|D=s'                =>\$dst_dir
    );

# Initial url 
#$url='http://www.onemanga.com/BLAME/1/01/';


my $mech = WWW::Mechanize->new(stack_depth => 2);

while (1)
{
  print STDERR "#" x 72, "\n" ;
  $mech->get($url);

  break unless $mech->success();

  my ($base_url) = $url =~  m{(http://[^/]+)/+};

  my ($next_url) = $mech->content =~
    m{<[[:space:]]*input[[:space:]]+
    type="button"[[:space:]]+
    value="next[[:space:]]+page"[[:space:]]
   onclick="javascript:window.location='([^']+)';"[[:space:]]*/*>}sx;
  
  $next_url = "$base_url/$next_url";

  $url =~ s/^$base_url//;

  my ($name, $chapter, $page) =  (split qq{/}, $url)[-3..-1] ;
  
# add leading zeros to $chapter and $page
  
  ($chapter)  = ('000' . $chapter)  =~ m/(.{3})$/;

# to enable that odd `double' pages
  $page =~ s/^[[:digit:]]+[^[:digit:]]//;

  ($page)     = ('000' . $page)     =~ m/(.{3})$/;

  my @dst_file = ($dst_dir,$name,$chapter,$page) ;

  shift @dst_file unless $dst_dir;

# create directories
  for (my $i=0; $i<$#dst_file;$i++)
  {
    my $dir =join('/',@dst_file[0..$i]);
    unless (-d $dir)
    {
      print STDERR "mkdir $dir\n";
      mkdir  $dir or die "$!" ;
    }
  }

  my %data =
  (
    'URI: '           => $mech->uri
    , 'URL: '         => $url
    , 'Base URI: '    => $mech->base 
    , 'Content : '    => $mech->ct
    , 'title : '      => $mech->title
    , 'Images : '     => join (qq/\n/, map { $_->url() } $mech->images )
    , 'Destination: ' => join ('/', @dst_file) 
    , 'Chapter : '    => $chapter
    , 'Page : '       => $page
    , 'Name : '       => $name
    , 'Next : '       => $next_url
  );

   
  print STDERR map { ($_ , $data{$_}, "\n") } sort {$a cmp $b} keys %data 
    if $debug;

  for ($mech->images) 
  {
    next unless ( $_->url =~ m{/mangas/.*(?:jpe?g|png|gif)$} );

    my ($extension) = $_->url =~ /(\.[^.]+)$/;
    $dst_file[$#dst_file] .= $extension;

    my $dst_file =join ('/', @dst_file);

    $mech->get($_->url) ;
    print STDERR qq/Downloading Image: `/, $_->url, qq/' to `$dst_file' .../;

    open DST , ">$dst_file" or die "\n$!";
    print DST $mech->content;
    close DST;

    print STDERR qq/successfull!\n/ if $mech->success; 

  }

  $url = $next_url;
}
